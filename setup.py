from os import path
from distutils.core import setup
import shutil 

here = path.abspath(path.dirname(__file__))
build_path = path.join(here,'build')

if path.isdir(build_path):
	print('Cleaning build')
	shutil.rmtree(path.join(here,'build'))
else:
	print('Nothing to clean')

setup(
    name='imagus-oauth2lib',
    
    # Versions should comply with PEP440.
    version='1.0.0',
    
    description='OAuth 2.0 compliant client and server library',
    long_description=open('README.md').read(),
    
    # The project's main webpage
    url='https://github.com/NateFerrero/oauth2lib',
    
    author='Nate Ferrero',
    author_email='nateferrero@gmail.com',
    
    license='LICENSE.md',
    
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 2',
        'Topic :: Security'
    ],
    
    keywords = 'oauth2',
    
    packages=['oauth2lib', 'oauth2lib.tests']
)
